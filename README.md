### 2017-11-26
* 备份chrome和opera安装包
* 
    `$ chromium-browser --ignore-certificate-errors`//ubuntu chromium-browser Chromium 62.0.3202.89 Built on Ubuntu , running on Ubuntu 17.04

    `$ chrome.exe  --ignore-certificate-errors`//windows chrome 51.0.2704.106

    `$ opera.exe  --ignore-certificate-errors`//windows Opera developer\39.0.2248.0

    `$ opera.exe  --ignore-certificate-errors`//windows Opera\49.0.2725.39
* 主要是安装包的体积相对于nproxy的代码文件有点大,就从[nproxy-rules](https://gitee.com/Snow_x_Connect/nproxy-rules)里删掉了 